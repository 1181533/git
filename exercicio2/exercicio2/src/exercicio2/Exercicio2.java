package exercicio2;

    import java.util.Scanner;

public class Exercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner ler = new Scanner(System.in);
        
        int numero=0, soma=0;
        
        System.out.println("Introduza um número.");
        numero = ler.nextInt();
        
        while (numero>100) {
            System.out.println("Introduza um número menor que 100.");
            numero = ler.nextInt();
        }

        for(int i = 0; i <= numero; i+=3) {
            soma = soma + i;
        } 
        System.out.print("Soma de números: " + soma);
    } 
}
